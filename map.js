// MAP.js based on leaflet.js
// map.js, map.html and map.css and map.cfg.example 
// v30012017.0001
//
// Copyright (c) 2015-2017, "Erich N. Pekarek" <erich@pekarek.priv.at>
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the FreeBSD Project.
//
function loadConfig(cfgURL){
	// get config file in plain text format and load it's variables
	cfg = new XMLHttpRequest();
	cfg.open('GET', cfgURL);
	cfg.responseType = 'text';
	cfg.addEventListener("load", function() { createConfig(cfg.responseText);});
	cfg.send(null);
	return 0;
}
function createConfig(configText){
	// prepare, check and load the config file contents into an object and initialize the map using the loaded config file data.
	var i = {};
	t = configText.split("\n");
	for (s in t){
		l=t[s].split("=");
		if (l.length>1) {
			o = l[1].replace(';','').trim();
			switch (l[0].trim()) {
				case 'centerMapAtLat':
					i["centerMapAtLat"] = parseFloat(o);
				break;
				case 'centerMapAtLon':
					i["centerMapAtLon"] = parseFloat(o);
				break;
				case 'nodesDBURL':
					if (o.length<255) i["nodesDbUrl"] = o;
				break;
				case 'communitySubDomain':
					if (o.length<255) i["communitySubDomain"] = o;
				break;
				case 'zoomLevel':
					i["zoomLevel"] = parseInt(o);
				break;
				case 'ToSfriendlyBehaviour':
					i["ToScompliant"] = new Boolean(parseInt(o));
				break;
				case 'googleMapsAPIKEY':
					if (o.length<255) i["googleMapsAPIKEY"] = o;
				break;
				case 'signupContact':
					if (o.length<255) i["signupContact"] = o;
				break;
				case 'extNodeInfoURL':
					if (o.length<255) {
						i["extNodeInfoURL"] = o;
					} else {
						i["extNodeInfoURL"] = '';
					}
				break;
				case 'mapBoxAPIKEY':
					if (o.length<255) i["mapBoxAPIKEY"] = o;
				break;
				case 'mapBoxProjectID':
					if (o.length<255) i["mapBoxProjectID"] = o;
				break;
				case 'bingAPIKEY':
					if (o.length<255) i["bingAPIKEY"] = o;
				break;
			}
		}
	}
	insertJSLib('https://maps.googleapis.com/maps/api/js?key=' + i.googleMapsAPIKEY);
	config = new Config(i);
	initMap(config);
}
function initMap(config){

	// initialize the map layer based on parsed settings, then get map objects from their configured data source.

	initializeMap(config.centerMapAt,config.zoomLevel,config.ToScompliant);
	
	loadData(config.nodesDbUrl);
	return;
}
function addMapLayerControls(){
	lc.addOverlay(ActiveNodes, "Active Nodes");
	lc.addOverlay(InactiveNodes, "Inactive Nodes");
	lc.addOverlay(SetupNodes, "Setup Nodes");
	lc.addOverlay(PotentialNodes, "Potential Nodes");
	lc.addOverlay(allLinksLayer, "Show All Links");
	map.addLayer(ActiveNodes);
	return;	
}
function sortSearchList(s){
	t = new Array();
	for (i=0; i < s.options.length; i++) {
	        t[i] = new Array();
	        t[i][0] = s.options[i].text.toLowerCase();
	        t[i][1] = s.options[i].value;
	}
	t.sort();
	while (s.options.length > 0) {
        	s.options[0] = null;
    	}
	delete i;
    	for (i=0; i < t.length; i++) {
        	o = new Option(t[i][0], t[i][1]);
        	s.options[i] = o;
    	}
}
function cleanSearchLists(){
	var u = document.getElementById('nodeSelector');
	if (!!u){
		while (u.firstChild) {
		    u.removeChild(u.firstChild);
		}
	}
	config.searchList = false;
	return;
}
function setPoints(coordinates){
	map.pointCounter++;
	icon = new L.divIcon({html: '<span class="pin Green"></span>'});
	if (map.pointCounter === 1) {
		marker1 = new L.marker(coordinates, {icon: icon, draggable: true}).addTo(toolsLayer);
		map.addLayer(toolsLayer);
	}	
	if (map.pointCounter === 2) {
		marker2 = new L.marker(coordinates, {icon: icon, draggable: true}).addTo(toolsLayer);
		initializeToolsLineLayer(Array(marker1.getLatLng(),marker2.getLatLng()));
		displayDistanceInfo();

		marker1.on("movestart", function(){
			toolsLineLayer.clearLayers();
			removeDistanceInfo();
		});
		marker1.on("move", function(){
			initializeToolsLineLayer(Array(marker1.getLatLng(),marker2.getLatLng())); 
		});
		marker1.on("moveend", function(){
			initializeToolsLineLayer(Array(marker1.getLatLng(),marker2.getLatLng()));
			displayDistanceInfo();
		});
		marker2.on("movestart", function(){ 
			toolsLineLayer.clearLayers();
			removeDistanceInfo();
		});
		marker2.on("move", function(){
			initializeToolsLineLayer(Array(marker1.getLatLng(),marker2.getLatLng()));
		});
		marker2.on("moveend", function(){
			initializeToolsLineLayer(Array(marker1.getLatLng(),marker2.getLatLng()));
			displayDistanceInfo();
		});
	}	
}
function getAngles(A, B){
	var aA = Math.atan2(B.lng - A.lng, B.lat - A.lat)/2/Math.PI*360;
	var aB = Math.atan2(A.lng - B.lng, A.lat - B.lat)/2/Math.PI*360;
	if (aA < 0) aA = aA + 360;
	if (aB < 0) aB = aB + 360;
	return Array(parseFloat(aA).toFixed(1),parseFloat(aB).toFixed(1));
}
function removeDistanceInfo(){
	marker1.unbindTooltip();
	marker2.unbindTooltip();
}
function displayDistanceInfo(){
	a=marker1.getLatLng();
	b=marker2.getLatLng();
	dist = a.distanceTo(b);
	angles = getAngles(a, b);
	marker1.bindTooltip('<div class="distinfo" style="text-align: center;">Distance: ' + distanceUnit(dist) + '<br/>Angle 1: ' + angles[0] + '° <br/>Location: '+ a.lat + '<br/> ' + a.lng + '</div>', { noHide: true }).openTooltip();
	marker2.bindTooltip('<div class="distinfo" style="text-align: center;">Distance: ' + distanceUnit(dist) + '<br/>Angle 2: ' + angles[1] + '° <br/>Location: '+ b.lat + '<br/> ' + b.lng + '</div>', { noHide: true }).openTooltip();
	return 0;
}
function destroyToolsLineLayer(){
	if (typeof toolsLineLayer != 'undefined'){
		toolsLineLayer.clearLayers();
		delete toolsLineLayer;
	}
	return 0;
}
function initializeToolsLineLayer(positions){
	destroyToolsLineLayer();
	toolsLineLayer = new L.layerGroup();
	line = new L.polyline(positions, {color: '#008000', weight: 2, smoothFactor: 1, opacity:1, dashArray: '5,1'}).addTo(toolsLineLayer);
	map.addLayer(toolsLineLayer);
}
function showSelectedNode(nodeValue) {
	var nodeRef;
	if (isNumeric(nodeValue)) {
		nodeRef = nodes.getNodeById(nodeValue);
	} else {
		nodeRef = nodes.getNodeByName(nodeValue);
	}
	if (!!nodeRef) {
		map.setView(nodeRef.coordinates,12);
		switch (nodeRef.nodeStatus) {
			case 'active':
				l=ActiveNodes;
			break;
			case 'inactive':
				l=InactiveNodes;
			break;
			case 'setup':
				l=SetupNodes;
			break;
			case 'potential':
				l=PotentialNodes;
			break;
			default:
				l=Array(ActiveNodes,InactiveNodes,SetupNodes,PotentialNodes);
			break;
		}
		map.addLayer(l);
		nodeRef.marker.openPopup();
	} else {
		alert('Sorry - data incomplete; try to reload');
	}
}
function loadData(nodeDataSource){
        var xml = new XMLHttpRequest();
	var method = 'GET';
	var params = null;
	var message = "reloading data from source - please wait";
	switch(config.datasourceAction){
		case 'refresh':
			console.log('refresh');
		break;
		case 'login':
			if ((config.username.length > 0) && (config.passwordhash.length > 8)) {
				method = 'POST';
				params = 'login=login&loginname=' + config.username + '&loginpassword=' + config.passwordhash;
				xml.withCredentials = true;
				config.sessionOwner = config.username;
				config.username = '';
				config.passwordhash = '';
			}
			console.log('login');
		break;
		case 'logout':
			params = 'action=logout';
			Cookies.remove('PHPSESSID', { path: '/' });
			config.sessionOwner = '';
			console.log('logout');
		break;
		default:
			message = "loading data from source - please wait";
			console.log('default');
		break;
	}

	xml.open(method, nodeDataSource , true);
	xml.addEventListener("loadstart", function() { userNotify(message); });
	xml.addEventListener("load", function() { createNodes(xml.responseXML); });
	xml.addEventListener("loadend", function() { closeUserNotify(); });
	xml.overrideMimeType('text/xml');
	xml.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	if (!!params) xml.setRequestHeader("Content-length", params.length);
	// xml.setRequestHeader("Connection", "close");
	if (debug) console.log("Method: " + method + " Params: " + params);
	xml.send(params);

	if (config.datasourceAction == 'logout') {
//		Cookies.remove('PHPSESSID', { path: '/' });
	}

	delete xml;
	prepareLogin(true);
	if (debug) console.log("Session Cookie is: " + Cookies.get('PHPSESSID'));
	if (debug) console.log("Session owner: " + config.sessionOwner);

	return 0;
}
function closeUserNotify(){
	while (un.firstChild) {
	    un.removeChild(un.firstChild);
	}
	un.blur();
	return;
}
function userNotify(text){
	var h = document.createElement("h1");
	h.innerHTML = text;
	un.focus();
	un.innerHTML = h.outerHTML;
	return;
}
function initializeMap(CenterPos, zoomLevel, ToScompliant){
	map = new L.Map('map', {zoomControl: false}).setView(CenterPos, zoomLevel);
	// initialize control elements
	// retrieve address on click
	geocoder = L.Control.Geocoder.nominatim({
		geocodingQueryParams:	{ addressdetails: 1, namedetails: 1 },
		reverseQueryParams:	{ addressdetails: 1 }
	});
	// Type address and Zoom to Map
        geocontrol = L.Control.geocoder({
		defaultMarkGeocode: false,
        	geocoder: geocoder,
		position: "topleft",
		placeholder: "search address",
        }).addTo(map);

	// map distance measurement tool and address click search map events.
	map.distmton = false;
	map.pointCounter = 0;
	
	map.on('click', function(e) {
		switch(map.distmton){
			case true:
				setPoints(e.latlng);
			break;
			case false:
				switch(map.address){
					case true:
						getAddress(e.latlng);
					break;
					case false:
						geocoderLayer.clearLayers();
					break;
				}
			break;
		}
	});


	zoom = new L.control.zoom({
		position: 'bottomright'
	}).addTo(map);


	// MAP LAYERS start here
	// OSM
	osmUrl = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
	osmAttrib = 'Map data &copy; OpenStreetMap contributors';
	osm = new L.TileLayer(osmUrl, {minZoom: 2, maxZoom: 23, attribution: osmAttrib});
	baseMaps["OSM"] = osm;

	// Google roads through googleMutant
	if (config.googleMapsAPIKEY !== 'undefined' ) {
		groads = L.gridLayer.googleMutant({
			attribution: 'Google Maps',
			type: 'roadmap'
		});
		baseMaps["Google Roads"] = groads;
	
		gsatellite = L.gridLayer.googleMutant({
			attribution: 'Google Maps',
			type: 'satellite'
		});
		baseMaps["Google Satellite"] = gsatellite;
	
		gterrain = L.gridLayer.googleMutant({
			attribution: 'Google Maps',
			type: 'terrain'
		});
		baseMaps["Google Terrain"] = gterrain;
	
		ghybrid = L.gridLayer.googleMutant({
			attribution: 'Google Maps',
			type: 'hybrid'
		});
		baseMaps["Google Hybrid"] = ghybrid;
	}

	if ((typeof config.mapBoxAPIKEY !== 'undefined') && (typeof config.mapBoxProjectID !== 'undefined')) {
		mapbox = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
			attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
			maxZoom: 18,	
			id: config.mapBoxProjectID,
	   		accessToken: config.mapBoxAPIKEY
		});
		baseMaps["Mapbox"] = mapbox;
	}

	if (typeof config.bingAPIKEY !== 'undefined') {
		bing = new L.BingLayer(config.bingAPIKEY, {type: imagerySet});
		baseMaps["Bing"] = bing;
	}

	// incompliant ToS Maps tiles workaround - to be replaced;
	if (ToScompliant == false) {
		// Bing
		Binglayer = new BingLayer('https://t{s}.tiles.virtualearth.net/tiles/a{q}.jpeg?g=1398', {
			subdomains: ['0', '1', '2', '3', '4'],
			attribution: '&copy; <a href="https://bing.com/maps">Bing Maps</a>'
		});
		baseMaps["Bing Layer"] = Binglayer;
	
		BinglayerRoad = new BingLayer('https://ecn.t{s}.tiles.virtualearth.net/tiles/h{q}.jpeg?g=2335', {
			subdomains: ['0', '1', '2', '3', '4'],
			attribution: '&copy; <a href="https://bing.com/maps">Bing Maps</a>'
		});
		baseMaps["Bing Road"] = BinglayerRoad;
	
		BinglayerStreets = new BingLayer('https://ecn.t{s}.tiles.virtualearth.net/tiles/r{q}.jpeg?g=2335&shading=hill', {
			subdomains: ['0', '1', '2', '3', '4'],
			attribution: '&copy; <a href="https://bing.com/maps">Bing Maps</a>'
		});
		baseMaps["Bing Streets"] = BinglayerStreets;
	}

	// data.wien.gv.at
	// http://www.basemap.at/wmts/1.0.0/WMTSCapabilities.xml
	wienBoxGeo = new L.tileLayer("https://maps{s}.wien.gv.at/basemap/geolandbasemap/normal/google3857/{z}/{y}/{x}.png", {
	    subdomains : ['', '1', '2', '3', '4'],
	    attribution: '&copy; <a href=\"http://data.wien.gv.at\">data.wien.gv.at</a>, <a href="http://www.basemap.at">Basemap.at</a>, <a href="http://www.isticktoit.net">isticktoit.net</a>'
});
	baseMaps["Geoland Basemap/wien.gv.at"] = wienBoxGeo;
	// -----
	wienBoxGeoOver = new L.tileLayer("https://maps{s}.wien.gv.at/basemap/bmapoverlay/normal/google3857/{z}/{y}/{x}.png", {
	    subdomains : ['maps', 'maps1', 'maps2', 'maps3', 'maps4'],
	    subdomains : ['', '1', '2', '3', '4'],
	    attribution: '&copy; <a href=\"http://data.wien.gv.at\">data.wien.gv.at</a>, <a href="https://www.basemap.at">Basemap.at</a>, <a href="http://www.isticktoit.net">isticktoit.net</a>'
});
	baseMaps["Geoland Basemap Overlay/wien.gv.at"] = wienBoxGeoOver;
	// -----
	wienBoxGeoOrtho = new L.tileLayer("https://maps{s}.wien.gv.at/basemap/bmaporthofoto30cm/normal/google3857/{z}/{y}/{x}.jpeg", {
	    subdomains : ['', '1', '2', '3', '4'],
	    attribution: '&copy; <a href=\"http://data.wien.gv.at\">data.wien.gv.at</a>, <a href="http://www.basemap.at">Basemap.at</a>, <a href="http://www.isticktoit.net">isticktoit.net</a>'
});
	baseMaps["Geoland Basemap Orthofoto/wien.gv.at"] = wienBoxGeoOrtho;
	// -----
	wienBoxGeoGrau = new L.tileLayer("https://maps{s}.wien.gv.at/basemap/bmapgrau/normal/google3857/{z}/{y}/{x}.png", {
	    subdomains : ['', '1', '2', '3', '4'],
	    attribution: '&copy; <a href=\"http://data.wien.gv.at\">data.wien.gv.at</a>, <a href="http://www.basemap.at">Basemap.at</a>, <a href="http://www.isticktoit.net">isticktoit.net</a>'
});
	baseMaps["Geoland Basemap Grau/wien.gv.at"] = wienBoxGeoGrau;
	// -----
	wienBoxGeoHDPI = new L.tileLayer("https://maps{s}.wien.gv.at/basemap/bmaphidpi/normal/google3857/{z}/{y}/{x}.jpeg", {
	    subdomains : ['', '1', '2', '3', '4'],
	    attribution: '&copy; <a href=\"http://data.wien.gv.at\">data.wien.gv.at</a>, <a href="http://www.basemap.at">Basemap.at</a>, <a href="http://www.isticktoit.net">isticktoit.net</a>'
});
	baseMaps["Geoland Basemap High DPI/wien.gv.at"] = wienBoxGeoHDPI;

	map.addLayer(osm);


	geocoderLayer = new L.layerGroup();
	// map distance measurement tool and address click search events - button actions.
	var b = document.getElementById('distmt');
	b.distmton = map.distmton;
	b.addEventListener("click", function(){
		b.distmton = !b.distmton;
		switch(b.distmton){
			case true:
				b.style.borderStyle = "inset";
				c.disabled=true;
				toolsLayer = new L.layerGroup();
				toolsLineLayer = new L.layerGroup();
				map.addLayer(toolsLayer);
				map.addLayer(toolsLineLayer);
				geocoderLayer.clearLayers();
			break;
			case false:
				b.style.borderStyle = "outset";
				c.disabled=false;
				map.removeLayer(toolsLineLayer);
				map.removeLayer(toolsLayer);
				map.pointCounter = 0;
				delete toolsLineLayer;
				delete toolsLayer;
			break;
		}
		map.distmton=b.distmton;
	});
	var c = document.getElementById('address');
	c.address = map.address;
	c.addEventListener("click", function(){
		c.address=!c.address;
		switch(c.address){
			case true:
				b.disabled=true;
				c.style.borderStyle = "inset";	
			break;
			case false:
				b.disabled=false;
				c.style.borderStyle = "outset";
				geocoderLayer.clearLayers();
				map.removeLayer(geocoderLayer);
			break;
		}
		map.address=c.address;
	});

	lc = new L.control.layers(baseMaps).addTo(map);
	addMapLayerControls();
	
}
function getAddress(coordinates){
	geocoderMarker = new L.layerGroup();
	geocoder.reverse(coordinates, map.options.crs.scale(map.getZoom()), function(results) {
		var r = results[0];	
		if (r) {
			if (geomarker) {
				geomarker.
				setLatLng(r.center).
				bindTooltip(r.html || r.bounds ).
				openTooltip();
			} else {
				geomarker = L.marker(r.center,{draggable: true}).bindTooltip(r.name).addTo(geocoderMarker).openTooltip();
		geomarker.on('dragend', function(){ getAddress(geomarker.getLatLng()); });
			}
		}
	});
	geocoderMarker.addTo(map);
}
function prepareLogin(onChange) {
	var loginDiv = document.getElementById("loginDrawer");
	var h = document.createElement("h1");
	var f = document.createElement("form");

	if (!!onChange) {
		while (loginDiv.firstChild) {
		    loginDiv.removeChild(loginDiv.firstChild);
		}
	}

	// come in and find out
	// f.action = 'javascript:function(){ return 0;}';
	f.addEventListener('submit', function(evt){ evt.preventDefault(); });
	f.method = "POST";
	if (debug) console.log("Session Owner: "+ config.sessionOwner);
	if (config.sessionOwner.length > 0) {
		h.innerHTML = 'Logout';
		var logoutaction = document.createElement("button");
		logoutaction.setAttribute("id","logout");
		logoutaction.setAttribute("name","logout");
		logoutaction.setAttribute("type","submit");
		logoutaction.innerHTML = 'Logout ' + config.sessionOwner;
		f.appendChild(logoutaction);
		f.onsubmit = function(){ submitLogout(); };
	} 
	if (config.sessionOwner.length == 0) {
		h.innerHTML = 'Login';
		f.setAttribute("id","loginForm");
		f.setAttribute("style","display:table; vertical-align:top;");
		
		var login = document.createElement("input");
		login.setAttribute("required","required");
		login.setAttribute("id","login");
		login.setAttribute("placeholder","your username");
	
		var password = document.createElement("input");
		password.setAttribute("required","required");
		password.setAttribute("id","password");
		password.setAttribute("type","password");
		password.setAttribute("placeholder","your password");
		
		var loginaction = document.createElement("button");
		loginaction.setAttribute("id","loginaction");
		loginaction.setAttribute("type","submit");
		loginaction.innerHTML = 'Login';
	
		f.appendChild(login);
		f.appendChild(password);
		f.appendChild(loginaction);
		f.onsubmit = function(){ submitCredentials(); };
	}
	loginDiv.appendChild(h);
	loginDiv.appendChild(f);
	
	/* refresh data */
	var rh = document.createElement("h1");
	var rf = document.createElement("form");
	var ra = document.createElement("button");
	ra.setAttribute("id","refresh");
	ra.setAttribute("name","refresh");
	ra.setAttribute("type","submit");
	ra.innerHTML = 'Reload data';
	rf.method = "POST";
	rf.appendChild(ra);
	rf.onsubmit = function(){ submitRefresh(); };
	rh.innerHTML = 'Reload';
	loginDiv.appendChild(rh);
	loginDiv.appendChild(rf);

	return 0;
}
function submitRefresh(){
	config.datasourceAction = 'refresh';
	loadData(config.nodesDbUrl);
	return 0;
}
function submitLogout(){
	config.datasourceAction = 'logout';
	loadData(config.nodesDbUrl);
	return 0;
}
function submitCredentials(){
	config.datasourceAction = 'login';
	config.username = document.getElementById('login').value;
	config.passwordhash = md5(document.getElementById('password').value);
	loadData(config.nodesDbUrl);
	return 0;
}
function prepareSaveSettings(){
	var x = document.getElementById('toolBox');
	var sf = document.createElement("form");
	sf.action = 'javascript:function() { return 0; }';
	sf.method = "POST";
	var h = document.createElement("h1");
	var saveButton = document.createElement("button");
	h.innerHTML = 'Save Settings';
	saveButton.innerHTML = 'Save';
	saveButton.setAttribute("type","submit");
	saveButton.setAttribute("id","saveSettings");
	sf.onsubmit = function(){submitSave();};
	sf.appendChild(saveButton);
	x.appendChild(h);
	x.appendChild(sf);
	return 0;
}
function submitSave(){
//	var bL = control.getActiveBaseLayer();
//	var oL = control.getActiveOverlayLayers();
//	console.log(bL);
//	console.log(oL);
	alert('This feature has not yet been implemented.');
	var idstore = [];   
	for(var x in map._layers) {
		// here idstore[x['polyid']] = x;
		console.log(map._layers[x]);
	}
	Cookies.set('activeLayers', 'active inactive setup potential');	
	Cookies.set('preferredMapLayer', 'gsatellite');	
	return 0;
}
Node.prototype.addNodeToSearchLists = function(){
	var a = document.getElementById('nodeSelector');
	var s = config.searchlists;
	var u = {};
	if (!config.searchList) {
		var h = document.createElement('h1');
		h.innerHTML = 'Nodes by status';
		a.appendChild(h);
	} 
	for (var i in s) {
		var j = s[i].name;
		u[j] = document.getElementById(j);
		if (!config.searchList) {
			if (u[j] === null) {
				u[j] = document.createElement('select');
				var v = document.createElement('option');
				v.text = s[i].realname;
				v.value = s[i].name;
				v.setAttribute('disabled', 'disabled');
				v.setAttribute('selected', 'selected');
				u[j].add(v);
				u[j].setAttribute('id',s[i].name);
				u[j].setAttribute('onchange', 'javascript:showSelectedNode(this.options[this.selectedIndex].value)');
				a.appendChild(u[j]);
			}
		}
	}
	if (Object.keys(u).length == config.searchlists.length) config.searchList = true;

	var o = document.createElement('option');
	if (this.name == this.id) {
		o.text = this.id;
	} else {
		o.text = this.name + ' (' + this.id + ')';
	}
	o.value = this.id;

	var p = document.createElement('option');
	p.text = o.text;
	p.value = o.value;

	switch(this.nodeStatus){
		case 'active':
			u.activeNodesList.add(o);
		break;
		case 'inactive':
			u.inactiveNodesList.add(o);
		break;
		case 'setup':
			u.setupNodesList.add(o);
		break;
		case 'potential':
			u.potentialNodesList.add(o);
		break;
	}
	u.allNodesList.add(p);
};
Node.prototype.generatePopupInfo = function(){
	nodeInfoTmp = this.createNodeInfo(Array(this.id, this.name, this.longitude, this.latitude, this.nodeStatus, this.type, this.tech_c, this.nodeIsStatic, this.myOwner, this.address, this.email, this.amountOfDevices, this.devices, this.amountOfLinks, this.links, this.notes ));
	this.nodeInfoHTML = document.createElement('div');
	this.nodeInfoHTML.innerHTML = nodeInfoTmp;
//	this.marker.bindPopup(this.nodeInfoHTML);
	
};
Node.prototype.colourLQ = function(LQ){
	switch (true){
		case (LQ > 4):
			c = 'red';
		break; 
		case (LQ >= 3):
			c = 'orange';
		break; 
		case (LQ >=2):
			c = 'yellow';
		break; 
		default:
			c = 'green';
		break;
	}
	return c;
};
Node.prototype.renderDistance = function(nodeRef1, nodeRef2) {
	r = '<div class="distance">' + distanceUnit(nodeRef1.coordinates.distanceTo(nodeRef2.coordinates)) + '</div>';
	return r;
};
function distanceUnit(distance){
	x = distance;
	y = '';
	r = '';
	if (x>=1000) {
		x = x/1000;
		y += ' km';
	} else {
		y += ' m';
	}
	/* round to two decimal points */
	x = +(Math.round(x + "e+2") + "e-2");
	r += x + ' ' + y + " ";
	return r;
}
function customIcon(nStatus, nType, nLinkCount){
// color values listed here represent a css class in map.css
	c = '';
	switch (nStatus) {
		case 'active':
			switch (nType) {
				case 'tunnel':
					c="Blue";
				break;
				case '5ghz':
					c="LightGreen";
				break;
				default:
					c="Red";
			}
		break;
		case 'inactive':
				c = "Darkgrey";
		break;
		case 'setup':
				c = "Grey";
		break;
		case 'potential':
				c = "White";
		break;
		default:
				c = "Red";
		break;
	}
	s = '';
	switch (true){
                case (nLinkCount>=30):
                        s = "size30plus";
                break;
                case (nLinkCount>=25):
                        s = "size25plus";
                break;
                case (nLinkCount>=20):
                        s = "size20plus";
                break;
		case (nLinkCount>=15):
			s = "size15plus";
		break;
		case (nLinkCount>=10):
			s = "size10plus";
		break;
		case (nLinkCount>=5):
			s = "size5plus";
		break;
		case (nLinkCount>=3):
			s = "size3plus";
		break;
		default:
			s = '';
		break;
	}
	this.icon = new L.divIcon({html: '<span class="pin ' + c + ' ' + s + '"></span>'});
}
Node.prototype.formatPopup = function(section, data){
	switch (section) {
		case 'nodeDetails':
			o = '' +
			'<div class="' + section + '">' +
			' <div>' +
			'  <div>Node ID:</div><div>' + data[0] + '</div>' +
			' </div>' +
			' <div>' +
			'  <div>Node Name:</div><div><a href="' + config.extNodeInfoURL.split("'")[1] + '=' + data[1] + '" target="_blank">' + data[1] + '</a></div>' +
			' </div>' +
			' <div>' +
			'  <div>Coordinates:</div><div style="word-wrap: break-word;">' + 
			'   <button id="copyCoords" onclick="javascript:copyToClipboard(document.getElementById(\'copyCoords\').innerHTML)">' + 
					data[3] + ' ' + data[2] + 
			'   </button>' +
			'  </div -->' +
			' </div>' +
			' <div>' +
			'  <div>Node Status:</div><div>' + data[4]+ '</div>' +
			' </div>' +
			' <div>' +
			'  <div>Node Type:</div><div>' + data[5] + '</div>' +
			' </div>';
			if (data[6]!==null){
				d=data[6].split("|");
			o += '' +
			' <div>' +
			'  <div>Node Technical Contact:</div><div><a href="mailto:' + d[0] + '%20%3c' + d[1] + '%3e?subject=Node: ' + encodeURI(data[1]) + '">' + d[0] +'</a></div>' +
			' </div>';
			}
			if (data[7]!==null){
			o += '' +
			' <div>' +
			'  <div>Node Static:</div><div>' + data[7]+ '</div>' +
			' </div>';
			}
			o += '' +
			'</div>';
		break;
		case 'nodeOwner':
			o = '' +
			'<div class="' + section + '">' +
			' <div>' +
			'  <div>Owner:</div>';
			if (data[1].length>0) {
				o += '<div>' + data[1] + '</div>';
			} else {
				o += '<div>Anonymized Data - Apply for an account at <a href="' + config.signupContactType + config.signupContactDestination + '">' + config.signupContactDestination + '</a></div>';
			}
			o += '' +
			' </div>' +
			' <div>' +
			'  <div>Address:</div>';
			if (data[2].length>0) {
				o += '<div>' + data[2] + '</div>';
			} else {
				o += '<div>anonymized</div>';
			}
			o += '' +
			' </div>' +
			' <div>' +
			'  <div>E-Mail:</div>';
			  if (data[1].length>0) { 
				o += '  <div><a href="mailto:' + data[1] + '%20%3c' + data[3] + '%3e?subject=Node: ' + encodeURI(data[0]) + '">' + data[3] +'</a></div>';
			  } else {
			        o += '  <div>&nbsp;</div>';
			  }
			o += '' +
			' </div>' +
 			'</div>'; 
		break;
		case 'nodeDevices':
			o = '' +
			'<div class="' + section + '">' +
			' <div>Amount of Devices: ' + data[0] + '</div>';
			  o += '<div>Devices: ';
			if (data[1].length===0){
				o += "invisible in anonymized data.";
			} else {
				o += '<table id="devices' + data[2] + '"' + ' class="sortable">';
				o += '	<thead>';
				o += '   <tr>';
				o += '	  <th>local device</th>';
				o += '	  <th>Interface IP</th>';
				o += '	  <th>OLSR info</th>';
				o += '   </tr>';
				o += '	</thead>';
				o += '	<tbody>';
					
				for (var i in data[1]){
					o += ' <tr>';
					var k=0;
				  	for (var j in data[1][i]){
						var d = data[1][i][j];
						switch (k) {
							case 0:
								o += '  <td class="normal">' + '<button onclick="javascript:window.open(\'http://' + d + '.' + config.communitySubDomain + '/\', \'_blank\')\">' + d.substring(0,d.indexOf(".")) + '</button>' + '</td>';
							break;
							case 1:
								o += '  <td class="normal">' + '<button onclick="javascript:window.open(\'http://' + d + '/\', \'_blank\')">' + d + '</a>' + '</td>'; 
							break;
							default:
								o += '  <td>' + d + '</td>';
							break;
						}
						k++;
					} 
					o += ' </tr>';
				}
				o += '	</tbody>';
	                        o += '</table>';
			}
			  o += ' </div>';
			 o += '</div>';
		break;
		case 'nodeLinks':
			o = '' +
			'<div class="' + section + '">' +
			' <div>Amount of Links: ' + data[0] + '</div>';
			  o += '<div>';
				o += '<table id="links' + data[2] + '" class="sortable">';
				o += '	<thead>';
				o += '   <tr>';
				o += '	  <th>local device</th>';
				o += '	  <th>partner device</th>';
				o += '	  <th>other node</th>';
				o += '	  <th>distance</th>';
				o += '	  <th>ETX</th>';
				o += '   </tr>';
				o += '	</thead>';
				o += '	<tbody>';
				for (var i in data[1]) {
					o += '	<tr>';
					var k=0;
					for (var j in data[1][i]) {
						var d = data[1][i][j];
						switch (k) {
							case 0:
								if (d.length == 0){
									o += '  <td class="normal">anonymized data</td>';
								} else {
									o += '  <td class="normal">' + '<button onclick="window.open(\'http://' + d + '.' + config.communitySubDomain + '/\',\'_blank\')">' + d.split('.')[0] + '</button>' + '</td>'; 
								}
							break;
							case 1:
								if (d.length == 0){
									o += '  <td class="normal">...</td>';
									o += '  <td class="normal">...</td>';
									o += '  <td class="normal">...</td>';
								} else {
									// o += '  <td class="normal">' + '<a href="http://' + d + '.' + config.communitySubDomain + '/">' + d.split('.')[0] + '</a>' + '</td>'; 
									o += '  <td class="normal">' + '<button onclick="window.open(\'http://' + d + '.' + config.communitySubDomain + '/\',\'_blank\')">' + d.split('.')[0] + '</button>' + '</td>'; 
	 								var f = d.split('.')[1];
									var t = nodes.getNodeByName(f);
									if (!!t) {
										o += '  <td class="normal"><button onclick="javascript:showSelectedNode(\'' + t.name + '\')">' + f + '</button></td>';
										o += '  <td class="normal">' + this.renderDistance(this,t) + '</td>';
									} else {
										o += '  <td class="normal">' + f + '</td>'; 
										o += '  <td class="normal">?</td>'; 
									}

								}
							break;
							case 3:
								o += '  <td class="' + this.colourLQ(d) + '">' + parseFloat(d).toFixed(3) + '</td>';
							break;
						}
						k++;
					} 
					o += '</tr>';
				}
				o += '	</tbody>';
			        o += '</table>';
			 o += ' </div>';
			 o += '</div>';
		break;
		case 'nodeNotes':
			if (data[0]!==null){
				o = '' +
				'<div class="' + section + '">' +
				' <div>Notes:' + data[0] + '</div>' +
				'</div>'; 
			} else {
				o = '';
			}
		break;
		default:
		break;
	}
	return o;
};
Node.prototype.createNodeInfo = function(infoArray){
	nodeInfoTypes = Array('nodeDetails','nodeOwner','nodeLinks','nodeNotes','nodeDevices');
	nodeInfo='';
	for (i in nodeInfoTypes){
		s = nodeInfoTypes[i];
		switch (s){
			case 'nodeDetails': 
				//id, name, latitude, longitude, nodeStatus, type, tech_c, nodeIsStatic));
				data = Array(infoArray[0], infoArray[1], infoArray[2], infoArray[3], infoArray[4], infoArray[5], infoArray[6], infoArray[7]);
			break;
			case 'nodeOwner':
				// name,owner,address,email
 				data = Array(infoArray[1], infoArray[8], infoArray[9], infoArray[10]);
			break;
			case 'nodeDevices':
				// amountOfDevices,devices));
				data = Array(infoArray[11], infoArray[12], infoArray[0]);
			break;
			case 'nodeLinks':
				// amountOfLinks,links
				data = Array(infoArray[13], infoArray[14], infoArray[0]);
			break;
			case 'nodeNotes':
				// notes
				data = Array(infoArray[15]);
			break;
			default:
				nodeInfo += '';
				// id, links
				data = Array(infoArray[0], infoArray[1], infoArray[14]);
			break;
		}
		nodeInfo += this.formatPopup(s, data);
	}
//	nodeInfoPrefix = '<div class="nodeinfo" id="node' + infoArray[0] + '"><h1>Node Information</h1>';
//	nodeInfoPostfix = '</div>';
//	nodeInfo = nodeInfoPrefix + nodeInfo + nodeInfoPostfix;
	nodeInfoDiv = document.createElement('div');
	nodeInfoDiv.setAttribute('id', 'node' + infoArray[0]);
	nodeInfoDiv.className = 'nodeinfo';
	var h = document.createElement('h1');
	h.innerHTML = 'Node Information';
	nodeInfoDiv.innerHTML = h.outerHTML.toString() + nodeInfo;
	
	return nodeInfoDiv.outerHTML.toString();
};
function createNodes(XMLData) {
	cleanSearchLists();
	N = XMLData.getElementsByTagName("node");
	for (var i = 0; i < N.length; i++) {
		j = N[i];
		id		= j.getAttribute('nodeid'),
		name		= j.getAttribute('name'),
		lat		= j.getAttribute('lat'),
		lon		= j.getAttribute('lon'),
		status		= j.getAttribute('type'),
		type		= j.getAttribute('nodetype'),
		ownername	= j.getAttribute('realname'),
		email		= j.getAttribute('email'),
		address		= j.getAttribute('address'),
		devices		= j.getAttribute('devices'),
		links		= j.getAttribute('links'),
		notes		= j.getAttribute('notes'),
		tech_c		= j.getAttribute('tech_c'),
		nodeIsStatic	= j.getAttribute('static');
		
		
		var nodeRef = nodes.getNodeById(id);
		if (!!nodeRef) {
			nodeRef.marker.removeFrom(map);
			nodes.removeById(nodeRef.id);
		} 
		x =  new Node(id, name, lat, lon, status, type, ownername, address, email, links, devices, notes, tech_c, nodeIsStatic);
		x.addNodeToSearchLists();	
		x.showLinks(allLinksLayer);
		nodes.addNode(x);
	}
	// htmlSearchListEvent();
	if (debug) console.log("Node array size: " + nodes.nodes.length);
}
Node.prototype.mouseOutNode = function(){
	map.removeLayer(nodeLinksLayer);
	nodeLinksLayer.clearLayers();
};
Node.prototype.mouseOverNode = function(){
	this.showLinks(nodeLinksLayer,true);
	this.showNodeName(nodeLinksLayer,true);
};
Node.prototype.showLinks = function (mapLayerRef, showLayer){
	if (this.links.length>0) {
		for (j in this.links){
			partner_node = this.links[j].partner_node;
			if ((Number.isInteger(partner_node)) && (this.links[j].remote_interface == '')) {
				t = nodes.getNodeById(partner_node);
			} else {
				t = nodes.getNodeByName(partner_node);
			}
		        switch (this.type) {
				case 'ethernet':
					c = '#444444';
				break;
				default:
					c = this.colourLQ(this.links[j].LQ);
				break;
			}
			if (t !== null) { 
				if (this.nodeIsStatic===1) {
					L.polyline(Array(this.coordinates, t.coordinates), {color: c, weight: 2, smoothFactor: 1, opacity:1, dashArray: '5,1'}).addTo(mapLayerRef);
				} else {
					L.polyline(Array(this.coordinates, t.coordinates), {color: c, weight: 2, smoothFactor: 1, opacity:1}).addTo(mapLayerRef);
				}
			}
		}
		if (showLayer === true){
			mapLayerRef.addTo(map);
		}
	}
};
Node.prototype.showNodeName = function (mapLayerRef, showLayer){
	this.marker.bindTooltip(this.name, { className: 'mouseOverNodeTooltip', Hide: true }).openTooltip();
//	this.marker.bindTooltip('<div class="nodename" style="text-align: center;">' + this.name, { className: 'mouseOverNodeTooltip', Hide: true }).openTooltip();
};
Node.prototype.getNodeInstanceByName = function(nodeName){
	for (i in nodes){
		if (nodes[i].name == nodeName){
			return nodes[i];
		}
	}
	return null;
};
Node.prototype.getNodeInstanceById = function(nodeId) {
	for (i in nodes){
		if (nodes[i].nodeid == nodeId) {
			return nodes[i];
		}
	}
	return null;
};
function Node(id, name, lat, lon, nodeStatus, nodeType, nodeOwner, address, email, links, devices, notes, tech_c, isStatic) {
	this.id = id;
	this.name = name;
	this.coordinates = new L.LatLng(lat, lon);
	this.longitude = lon;
	this.latitude = lat;
	this.nodeStatus = nodeStatus;
	this.type = nodeType;
	this.myOwner = nodeOwner;
	this.address = address;
	this.email = email;
	this.links = [];
	this.devices = [];
	this.notes = notes;
	this.tech_c = tech_c;
	this.nodeIsStatic = isStatic;
	this.overlay = [];
	if (devices.length>0) {
		s = devices.split('|');
		for (i in s) {
			t = s[i].split(',');
			this.devices[i] = [];
			this.devices[i]['name'] = t[0];
			this.devices[i]['ip'] = t[1];
			this.devices[i]['seen'] = t[2];
			delete t;
		}
		delete i;
		delete s;
	}
	this.amountOfLinks = links.split('|').length;
	this.amountOfDevices = devices.split('|').length;
	if (links.length>0) {
		s = links.split('|');
		for (i in s) {
			t = s[i].split(',');
			this.links[i] = [];
			this.links[i]['local_interface'] = t[0];
			this.links[i]['remote_interface'] = t[1];
			this.links[i]['partner_node'] = t[2];
			this.links[i]['LQ'] = t[3];
			delete t;
		}
		delete i;
		delete s;
	}

	this.updated = Date.now();
//	this.createPopupInfo();
	this.createMarkerEvents();
}
Node.prototype.createMarkerEvents = function(){
	var layer;
	this.icon = new customIcon(this.nodeStatus, this.type, this.amountOfLinks);
	this.nodeMarker = L.Marker.extend({
		parent: this,
		riseOnHover: true
	});
	switch (this.nodeStatus) {
		case 'active':
			layer=ActiveNodes;
		break;
		case 'inactive':
			layer=InactiveNodes;
		break;
		case 'setup':
			layer=SetupNodes;
		break;
		case 'potential':
			layer=PotentialNodes;
		break;
		default:
		break;
	}
	this.marker = new this.nodeMarker(this.coordinates, this.icon, this.name).addTo(layer);
	this.marker.on("mouseover", function (){ this.parent.mouseOverNode(); });
	this.marker.on("mouseout", function (){ this.parent.mouseOutNode(); });
	this.marker.bindPopup('');
	// this.update: see L.popup.update();
	// this.parent: Node instance;
	this.marker.on('popupopen', function() { 
		this.parent.updatePopupInfo(this.parent); 
		var l = this.parent.nodeInfoHTML.getElementsByClassName('sortable');
		for (var i=0; i<l.length; i++){
			sorttable.makeSortable(l[i]);
		}
		this.parent.marker.setPopupContent(this.parent.nodeInfoHTML);
	});
}
Node.prototype.updatePopupInfo = function(n){
	this.nodeInfoHTML = document.createElement('div');
	var popupInfo = this.createNodeInfo(Array(
		this.id,
		this.name,
		this.longitude,
		this.latitude,
		this.nodeStatus,
		this.type,
		this.tech_c,
		this.nodeIsStatic,
		this.myOwner,
		this.address,
		this.email,
		this.amountOfDevices,
		this.devices,
		this.amountOfLinks,
		this.links,
		this.notes
	));
	this.nodeInfoHTML.innerHTML = popupInfo;
}
Node.prototype.createPopupInfo = function(){
	var popupInfo = this.createNodeInfo(Array(this.id, this.name, this.longitude, this.latitude, this.nodeStatus, this.type, this.tech_c, this.nodeIsStatic, this.myOwner, this.address, this.email, this.amountOfDevices, this.devices, this.amountOfLinks, this.links, this.notes ));
	this.nodeInfoHTML = document.createElement('div');
	this.nodeInfoHTML.innerHTML = popupInfo;
	

	var l = this.nodeInfoHTML.getElementsByClassName('sortable');
	for (var i=0; i<l.length; i++){
		sorttable.makeSortable(l[i]);
	}
	
}
function Nodes(){
	this.nodes = [];
	this.ActiveNodes = [];
	this.InactiveNodes = [];
	this.SetupNodes = [];
	this.PotentialNodes = [];
}
Nodes.prototype.addNode = function(NodeObj) {
	this.nodes.push(NodeObj);
	switch(NodeObj.nodeStatus) {
		case 'active':
			this.ActiveNodes.push(Array(NodeObj.id,NodeObj.name));
		break;
		case 'inactive':
			this.InactiveNodes.push(Array(NodeObj.id,NodeObj.name));
		break;
		case 'setup':
			this.SetupNodes.push(Array(NodeObj.id,NodeObj.name));
		break;
		case 'potential':
			this.PotentialNodes.push(Array(NodeObj.id,NodeObj.name));
		break;
		default:
		break;
	}
};
Nodes.prototype.removeById = function(NodeId) {
	const pos = this.nodes.findIndex(item => item.id === NodeId);
	this.nodes.splice(pos, 1);
	// if (debug) console.log("Removing Node with NodeId=" + NodeId + ".");
};
Nodes.prototype.getNodeByName = function(Name) {
	for (var i in this.nodes) {
		if (this.nodes[i].name === Name) {
			return this.nodes[i];
		}
	}
	return null;
};
Nodes.prototype.getNodeById = function(Id) {
	for (var i in this.nodes) {
		if (this.nodes[i].id === Id) {	
			return this.nodes[i];
		}
	}
	return null;
};
Nodes.prototype.sort = function() {
	this.nodes.sort(function(a, b){
		nameA = a.name.toLowerCase();
		nameB = b.name.toLowerCase();
		if (nameA < nameB) //sort string ascending
			return -1;
		if (nameA > nameB) 
			return 1;
		return 0 //default return value (no sorting)
	})
};
function Config(i){
	this.nodesDbUrl = i.nodesDbUrl;
	this.communitySubDomain = i.communitySubDomain;
	if (typeof i.centerMapAtLat !== 'undefined') {
		if (typeof i.centerMapAtLon !== 'undefined') {
			this.centerMapAt = new L.LatLng(i.centerMapAtLon,i.centerMapAtLat);
		}
	} else {
		this.centerMapAt = new L.LatLng(16,48);
	}
	if (this.nodesDbUrl) {
		this.configured = true;
	} else {
		this.configured = false;
	}
	this.zoomLevel = i.zoomLevel;
	this.ToScompliant = i.ToScompliant;
	if (typeof i.googleMapsAPIKEY !== 'undefined') this.googleMapsAPIKEY = i.googleMapsAPIKEY;
	if (typeof i.mapBoxAPIKEY !== 'undefined') this.mapBoxAPIKEY = i.mapBoxAPIKEY;
	if (typeof i.mapBoxProjectId !== 'undefined') this.mapBoxProjectId = i.mapBoxProjectId;
	if (typeof i.bingAPIKEY !== 'undefined') this.bingAPIKEY = i.bingAPIKEY;
	if ((!!i.signupContact) && (i.signupContact.length>0)) {
		switch(i.signupContact.split(':')[0]){
			case 'web':
				this.signupContactType='http://';
			break;
			case 'webs':
				this.signupContactType='https://';
			break;
			case 'email':
				this.signupContactType='mailto:';
			break;
			default:
				this.signupContactType='';
			break;
		}
		this.signupContactDestination = i.signupContact.split(':')[1];
	} else {
		this.signupContactType='';
		this.signupContactDestination='#';
	}
	if (i.extNodeInfoURL != null) {
		this.extNodeInfoURL = i.extNodeInfoURL;
	}
	this.username = '';
	this.passwordhash = '';
	this.sessionOwner = '';
	this.logout = false;
	this.datasourceAction = '';
	this.activeLayers = Cookies.get("ActiveLayers");
	this.searchlists = new Array( 
				{ name: 'activeNodesList', realname: 'Active Nodes' },
				{ name: 'inactiveNodesList', realname: 'Inactive Nodes' },
				{ name: 'setupNodesList', realname: 'Setup Nodes' },
				{ name: 'potentialNodesList', realname: 'Potential Nodes' },
				{ name: 'allNodesList', realname: 'All Nodes' }
			);
	this.searchList = false;
}
function main(){
	un = document.getElementById('userNotify');
	if (debug) console.log("node array: "+nodes.nodes.length);
	host=top.location.host
	loadScripts();
	loadConfig('map.cfg');
	prepareSaveSettings();
	sorttable.init();
		
}
debug = true;
config = {};
nodes = new Nodes();
lc = {};
baseMaps = {};
ActiveNodes = L.layerGroup();
InactiveNodes = L.layerGroup(); 
SetupNodes = L.layerGroup(); 
PotentialNodes = L.layerGroup();
allLinksLayer = L.layerGroup();
nodeLinksLayer = L.layerGroup();
var geocoder;
var geocontrol;
var geomarker;
window.onload = main;
